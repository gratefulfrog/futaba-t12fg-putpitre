$fn=100;

DXFFilename        = "../DXF/back-plate 03.dxf";
contourLayeName    = "Contour";
innerCutsLayerName = "inner_contours";

plateZ = 4;

difference(){
linear_extrude(plateZ)
  import(DXFFilename,layer=contourLayeName);
  
linear_extrude(plateZ)
  import(DXFFilename,layer=innerCutsLayerName);
}